# Basics

A project with functions for most of the basic C functionality.

Compile and run in a POSIX complaint, C environment.

Using, WinSCP and PuTTy to connect to cs-linux.ncl.ac.uk via SSH.

To run:
- cd to repo
- cc -Wall basics.c -o basics
- ./basics
