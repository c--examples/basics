#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int string(){
    char name[] = "Ben";
    int age = 19;
    printf("Hello %s, age: %d \n", name,age);
    return 0;
}

int dataTypes(){
    int age = 19;
    double age2 = 19.5;
    char grade = 'A';
    //string = "", char = ''
    char phrase[] = "hello";
    // use a format specifier for each data type
    printf("%d\n",age);
    printf("%f\n",age2);
    printf("%c\n",grade);
    printf("%s\n",phrase);
    // can do operators here
    printf("%f\n", 5.0*3.7);
    return 0;
}

int mathOperators(){
    // powers
    printf("%f\n", pow(2,3));
    // sqrt
    printf("%f\n", sqrt(16));
    //round up
    printf("%f\n", ceil(23.23));
    //round down
    printf("%f\n", floor(23.99));
    return 0;
    /*comment
            block
    nice*/
}

int constants(){
    const int NUM = 123;
    printf("%d\n",NUM);
    // use CAPS for name

    return 0;
}

int inputs(){
    /*
     * scanf - get a datatype, only works upto space
     *
     * Only need &(pointer) when not a string
     */
    int age;
    printf("Enter your age: \n");
    scanf("%d", &age);
    printf("%d\n", age);
    // for double change %d to %lf

    char name[20];
    printf("Enter your name: \n");
    scanf("%s%*c", name);
    // add %*c so that it ignores the new line from pressing enter
    printf("Your name: %s\n", name);
    /*
     * fgets - get a line
     */

    char name2[20];
    printf("Enter your full name: \n");
    fgets(name2, 20, stdin);
    printf("Your full name: %s",name2);

    return 0;
}

int addCalc(){
    double num1;
    double num2;
    printf("Enter first number: ");
    scanf("%lf", &num1);
    // when scanning for float, need long float
    printf("Enter second number: ");
    scanf("%lf", &num2);
    printf("Num1 + Num2 = %lf\n", num1 + num2);
    return 0;
}



int main(){
    //string();
    //dataTypes();
    //mathOperators();
    //inputs();
    addCalc();
    return 0;
}